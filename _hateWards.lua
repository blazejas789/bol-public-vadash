local blackColor  = 4278190080
local purpleColor = 4294902015
local greenColor  = 4278255360
local yellowColor = 4294967040

function OnRecvPacket(p)
	if p.header == 0x07 then
		p.pos = 1
		local deaddid = p:DecodeF()
		local killerid = p:DecodeF()
		for networkID, ward in pairs(placedWards) do
			if ward and deaddid and networkID == deaddid and killerid == 0 then
				--print("ward has been deleted")
				placedWards[networkID] = nil
			end
		end
	end	
	if p.header == 0xB5 then
		p.pos = 12
		local wardtype2 = p:Decode1()
		p.pos = 1
		local creatorID = p:DecodeF()
		p.pos = p.pos + 28
		local creatorID2 = p:DecodeF()
		local objectID = p:DecodeF()
		local objectX = p:DecodeF()
		local objectY = p:DecodeF()
		local objectZ = p:DecodeF()
		local objectX2 = p:DecodeF()
		local objectY2 = p:DecodeF()
		local objectZ2 = p:DecodeF()
		p.pos = p.pos + 4
		local warddet = p:Decode1()
		p.pos = p.pos + 4
		local warddet2 = p:Decode1()
		p.pos = 15
		local wardtype = p:Decode1()
		local visiontype
		local objectID = DwordToFloat(AddNum(FloatToDword(objectID), 2))
		local creatorchamp = objManager:GetObjectByNetworkId(creatorID)
		local duration
		local range
		--print("Ward type  : "..wardtype.."\n Ward Type2 : "..wardtype2.."\n WardDet : "..warddet)
		if creatorchamp and creatorchamp.team == myHero.team and WardsHater.ownteam == false then
			return
		end
		if wardtype == 0x0D or wardtype == 0x06 then
			visiontype = false
		else visiontype = true
		end
		if warddet == 0x3F and warddet2 == 0x33 and wardtype ~= 12 then --wards
			placedWards[objectID] = {x = objectX2, y = objectY2, z = objectZ2, visionRange = 1100, color = (visiontype and purpleColor) or greenColor, spawnTime = GetTickCount(), duration = (wardtype2 == 0xB4 and 60000) or 180000 }
		end
	end
	p.pos = 1
end

function OnLoad()
	local loadedTable, error = Serialization.loadTable(SCRIPT_PATH .. 'Common/IHateWards_cache.lua')
	if not error and loadedTable.saveTime <= GetInGameTimer() then
		placedWards = loadedTable.placedWards
	else
		placedWards = {}
	end
	WardsHater = scriptConfig("I hate your Wards", "IHYW")
	WardsHater:addParam("ownteam", "Display own team objects(testing purpose)", SCRIPT_PARAM_ONOFF, false)
	PrintChat(" >> I hate your Wards 3.13")
end

function OnUnload()
	Serialization.saveTable({placedWards = placedWards, saveTime = GetInGameTimer()}, SCRIPT_PATH .. 'Common/IHateWards_cache.lua')
end

function OnDraw()
	for networkID, ward in pairs(placedWards) do
		if ward ~= nil and (GetTickCount() - ward.spawnTime) > ward.duration then
			placedWards[networkID] = nil
		elseif ward ~= nil then
			local minimapPosition = GetMinimap(ward)
			DrawTextWithBorder('.', 60, minimapPosition.x - 3, minimapPosition.y - 43, ward.color, blackColor)

			local x, y, onScreen = get2DFrom3D(ward.x, ward.y, ward.z)
			DrawTextWithBorder(TimerText((ward.duration - (GetTickCount() - ward.spawnTime)) / 1000), 20, x - 15, y - 11, ward.color, blackColor)

			DrawCircle(ward.x, ward.y, ward.z, 90, ward.color)
			if IsKeyDown(16) then
				DrawCircle(ward.x, ward.y, ward.z, ward.visionRange, ward.color)
			end
		end
	end
end

function DrawTextWithBorder(textToDraw, textSize, x, y, textColor, backgroundColor)
	DrawText(textToDraw, textSize, x + 1, y, backgroundColor)
	DrawText(textToDraw, textSize, x - 1, y, backgroundColor)
	DrawText(textToDraw, textSize, x, y - 1, backgroundColor)
	DrawText(textToDraw, textSize, x, y + 1, backgroundColor)
	DrawText(textToDraw, textSize, x , y, textColor)
end